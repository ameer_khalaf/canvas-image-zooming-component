/**
 * Image zoom component
 * to initialize the app call ImageZoomComponent.init(options:Object); under the script tag
 * option object is required
 * allowed properties
 * {
 *  parentEl: element, //"ref to element on the screen where the canvas will be printed"
 *  fullScreen: boolean, // use to set the canvas to the width and height of the view port
 *  fixSize: boolean, // use this flag to set the canvas to fixed width and height
 *  matchParentSize: boolean, // use this flag to make the canvas match the parent element size
 *  width: integer, // set the canvas width use when fixSize flag is true default value is 300
 *  height: integer // set the canvas height use when fixSize flag is true default value is 300
 *  }
 *
 *
 */

(function (windows, document, ImageZoomComponent) {

    let zoom = 0;
    const zoomScale = [1, 4, 16, 64]; // this value should be provided by backend, used to calc the tiling grid.
    const tileSize = 256; // tile size and type can be passed down as config for this project they will be hardcoded
    const imageType = "jpg";
    const providerUrl = "data/tiled/"; // this value can be provided by init setup or backend base on widget key.
    let canvasWidth = 300; // default value
    let canvasHeight = 300; // default value (windows.innerHeight - 20);
    let fullScreen;  // flag to set canvas to the screen size
    let matchParentSize;
    let canvas;
    let ctx;
    let mouseXY;
    let mouseBaseXY;
    let canvasPosition;
    let isMouseDown = false;
    let baseXY = {
        x: (canvasWidth / 2),
        y: (canvasHeight / 2)
    };
    let tileList = [];

    /*** helper functions  **/

    function validateInt(input) {
        let intNumber = parseInt(input);
        // (intNumber !== intNumber) returns true only for NaN this is to check against null values;
        if ((intNumber !== intNumber) || isNaN(input)) {
            return null;
        }
        return intNumber;
    }

    function getPosition(el) {
        var xPosition = 0;
        var yPosition = 0;

        while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
            xPosition += (el.offsetLeft - el.scrollLeft + el.clientLeft);
            yPosition += (el.offsetTop - el.scrollTop + el.clientTop);
            el = el.offsetParent;
        }
        return {
            x: xPosition,
            y: yPosition
        };
    }

    function getMousePosition(event) {
        let mouseX = event.clientX - canvasPosition.x;
        let mouseY = event.clientY - canvasPosition.y;
        return {
            x: mouseX,
            y: mouseY
        }

    }

    // allow us to reduce the amount of calls made be the events handlers.
    function throttle(func, interval) {
        var timeout;
        return function () {
            var context = this, args = arguments;
            var later = function () {
                timeout = false;
            };
            if (!timeout) {
                func.apply(context, args);
                timeout = true;
                setTimeout(later, interval)
            }
        }
    }

    /**
     * this function act as a backend url generation
     * it take a zoomLevel and return a multidimensional array reflecting the zoom level gird.
     * @param zoomLevel
     * @returns {Array}
     */
    async function getImageUrl(zoomLevel) {
        return new Promise(resolve => {
            let baseUrl = providerUrl + zoomLevel;
            let gridCells = zoomScale[zoomLevel];
            // to manage zoom level 0  use Math.ceil
            let xColumns = Math.sqrt(gridCells);
            let pathList = [];
            for (let i = 0; i < xColumns; i++) {
                let path = baseUrl + "/" + i;
                let colTiles = [];
                for (let cell = 0; cell < xColumns; cell++) {
                    let cellPath = path + "/" + cell + "." + imageType;
                    colTiles.push(cellPath);

                }
                pathList.push(colTiles);
            }
            resolve(pathList);
        });
    }

    /**
     * Create an image tile in the grid
     * @param x
     * @param y
     * @param imageUrl
     */
    function TileImage(x, y, imageUrl, tileSize) {
        this.width = tileSize;
        this.height = tileSize;
        this.x = x;
        this.y = y;
        this.imageUrl = imageUrl;
        this.img = new Image();
        this.img.src = this.imageUrl;
    }

    function drawColumnCells(xPosition, yPosition, numberOfCells, imagePath) {
        let tileColumn = [];
        for (let i = 0; i < numberOfCells; i++) {
            let y = yPosition + (tileSize * i);
            let image = new TileImage(xPosition, y, imagePath[i], tileSize);
            tileColumn.push(image);
        }
        return tileColumn;
    }

    async function createGrid(zoomLevel, xCenter, yCanter, xColumns) {
        let imagePath = await getImageUrl(zoomLevel);
        let resultList = [];
        for (let i = 0; i < xColumns; i++) {
            let xPosition = xCenter + (tileSize * i);
            let colCells = drawColumnCells(xPosition, yCanter, xColumns, imagePath[i]);
            resultList.push(colCells);
        }
        return resultList;
    }

    function updateGrid(xCenter, yCanter, list) {

        list.forEach((col, index) => {
            let xPosition = xCenter + (tileSize * index);
            col.forEach((tile, cellIndex) => {
                tile.x = xPosition;
                tile.y = yCanter + (tileSize * cellIndex);
                ;
            })
        });

        return list;

    }

    async function drawGrid(zoomLevel, xBasePosition, yBasePosition) {

        let gridCells = zoomScale[zoomLevel];
        let xColumns = Math.sqrt(gridCells);

        let centerX = xBasePosition - ((xColumns * tileSize) / 2);
        let centerY = yBasePosition - ((xColumns * tileSize) / 2);
        if ((zoomLevel !== zoom) || (tileList.length !== xColumns)) {
            tileList = await createGrid(zoomLevel, centerX, centerY, xColumns);
        } else {
            tileList = updateGrid(centerX, centerY, [...tileList]);
        }
        tileList.forEach(col => {
            col.forEach(item => {
                if (item.img.complete) {
                    ctx.drawImage(item.img, item.x, item.y);
                }
            })
        })

    }

    function updateZoom() {
        baseXY.x = (canvasWidth / 2);
        baseXY.y = (canvasHeight / 2);
        updateCanvas();
    }

    function zoomIn() {
        if (zoom < (zoomScale.length - 1)) {
            zoom += 1;
            updateZoom();
        }
    }

    function zoomOut() {
        if (zoom > 0) {
            zoom -= 1;
            updateZoom();
        }
    }

    function createZoomPanel(parentEl) {
        const btnStyle = "border-radius: 2px;display: block;height: 29px;left: 0px;overflow: hidden;cursor: pointer;width: 29px;z-index: 2;box-sizing: border-box;transition: background-color 0.16s ease-out;font-size: 16px;";
        let wrapper = document.createElement("div");
        wrapper.style.cssText = "position: absolute; top: 10px; left: 10px; border: 1px solid rgba(201, 201, 201, 0.3); overflow: hidden; padding: 2px; display: block;";

        let zoomInBtn = document.createElement("BUTTON");
        let zoomInBtnText = document.createTextNode("+");
        zoomInBtn.appendChild(zoomInBtnText);
        zoomInBtn.style.cssText = btnStyle;

        let zoomOutBtn = document.createElement("BUTTON");
        let zoomOutBtnText = document.createTextNode("-");
        zoomOutBtn.appendChild(zoomOutBtnText);
        zoomOutBtn.style.cssText = btnStyle;

        zoomInBtn.addEventListener("click", zoomIn, false);
        zoomOutBtn.addEventListener("click", zoomOut, false);

        wrapper.appendChild(zoomInBtn);
        wrapper.appendChild(zoomOutBtn);
        parentEl.style.position = "relative";
        parentEl.appendChild(wrapper);
    }

    function updateCanvas() {
        ctx.clearRect(0, 0, canvasWidth, canvasHeight);
        drawGrid(zoom, baseXY.x, baseXY.y);
        requestAnimationFrame(updateCanvas);
    }

    function panningLogic() {
        isMouseDown = false; // to prevent panning from triggering on init;
        // mouse events for panning
        canvas.addEventListener("mousedown", function (e) {
            isMouseDown = true;
            mouseBaseXY = getMousePosition(e);
        }, false);
        canvas.addEventListener("mouseup", function (e) {
            isMouseDown = false;
        }, false);
        canvas.addEventListener("mouseout", function (e) {
            isMouseDown = false;
        }, false);
        canvas.addEventListener("click", function (e) {
            isMouseDown = false;
        }, false);
        canvas.addEventListener("mousemove", throttle(function (e) {
            // to prevent panning on regular mouse movement only on hold.
            if (!isMouseDown) {
                return;
            }
            mouseXY = getMousePosition(e);
            baseXY.x = baseXY.x + (mouseXY.x - mouseBaseXY.x);
            baseXY.y = baseXY.y + (mouseXY.y - mouseBaseXY.y);
            updateCanvas();
        }, 200), false);

    }

    function init() {
        canvasPosition = getPosition(canvas);
        // set app elements
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
        ctx = canvas.getContext("2d");

        panningLogic();
        updateZoom();
        // requestAnimationFrame(updateCanvas);
    }

    function createWidget(options) {
        // setup app values
        // default parent element is body
        let parentEl = options.parentEl ? options.parentEl : document.querySelector("body");
        if (options.fullScreen === true) {
            fullScreen = true;
            canvasWidth = (windows.innerWidth - (windows.scrollX + 5));
            canvasHeight = (windows.innerHeight - (windows.scrollY + 5));
        }
        if (options.fixSize === true) {
            let intWidth = validateInt(options.width);
            let intHeight = validateInt(options.height);

            canvasWidth = (intWidth !== null) ? intWidth : canvasWidth;
            canvasHeight = (intHeight !== null) ? intHeight : canvasHeight;
        }
        if (options.matchParentSize) {
            matchParentSize = true;
            canvasWidth = options.parentEl.clientWidth - 2;
            canvasHeight = options.parentEl.clientHeight - 2;
        }
        canvas = document.createElement("canvas");
        canvas.style.cssText = "position:absolute; left:0px; top:0px; border: 1px solid rgba(201, 201, 201, 0.57); cursor: move;";
        canvas.id = "imageViewer";  // todo: make the id unique to prevent conflict
        parentEl.appendChild(canvas);
        createZoomPanel(parentEl);
        init();

        // add resize listner to re-draw the component
        window.addEventListener('resize', function (e) {
            if (fullScreen) {
                canvasWidth = (windows.innerWidth - (windows.scrollX + 5));
                canvasHeight = (windows.innerHeight - (windows.scrollY + 5));
            }

            if (matchParentSize) {
                canvasWidth = options.parentEl.clientWidth - 2;
                canvasHeight = options.parentEl.clientHeight - 2;
            }
            init()

        });

    }


    ImageZoomComponent.init = function (options) {
        createWidget(options)
    };


})(window, document, window.ImageZoomComponent = window.ImageZoomComponent || {});


