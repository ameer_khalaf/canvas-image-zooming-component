Propeller Front End Coding Challenge
====================================

#Overview 

This is a simple javascript component that allow user to view image and zoom in and out. 

the component offer the following functions:
- zoom in and out (3 levels)
- image Panning, hold and drag the image around the canvas
- component can be placed and resized vi options based on user requirement "see usage section"  

## Design notes 

I have implement the component using plain js and html5 canvas, 
as my understanding of the task requirements outlined in the PROBLEM.md file, 

- That the solution should show JS coding strengths without relaying on frameworks or libraries. 

- Also if it can be done with plain js then why not. 

Over all working with canvas, images and native javascript and without the comfort of supporting framework was interesting. 

### the component design: 
The component is a single js file that can be initialized from "window.ImageZoomComponent" with a list of options. (see usage section for config options) 

- The code style mixes ES5 with ES6 feature, mainly for compatibility and to avoid using a build tool.
- The script will append a canvas to the parent element. 
- The canvas controller will create a grid based on the zoom level selected. 
- The image-tiles url retrieval was designed in a way that it behave as if urls will come from backend.   
- The canvas controller will draw the image tiles always at the center of the canvas if tile grid is bigger than the canvas the remain tiles will be hidden 
- The canvas controller allow panning: moving the image base on the mouse movement while holding. 

### Implementation challenges 
  
- panning: sync the mouse event with image positions and re-draw grid: 
   - I also used the native  "requestAnimationFrame(updateCanvas);" but that did not help at the beginning  
   - To give a smooth transition I used throttle fuc to reduce the number of re-draw while moving the mouse. 
   - I also change implementation of the grid drawing to optimise the number of time we load image from urls.
   
- loading tile image within the view only. I know its possible to do this by: 
    - Checking when create the tile if its not within the view port then we ignore draw, 
    - Also when init the tile class we set image src to dummy image. Then when the image is the view port we set the src to the correct url;   


## Usage 

to use the component please follow these steps;

- Add imageZoomComponent to your js directory 
- Insert the file in the "body" or the "header" section of your html 
- Add a scrip tag as outlined in the html "body" to initialize the component.



**Options object is required** :
 Allowed properties
 *  parentEl: element, //"ref to element on the screen where the canvas will be printed"
 *  fullScreen: boolean, // use to set the canvas to the width and height of the view port
 *  fixSize: boolean, // use this flag to set the canvas to fixed width and height
 *  matchParentSize: boolean, // use this flag to make the canvas match the parent element size
 *  width: integer, // set the canvas width use when fixSize flag is true default value is 300
 *  height: integer // set the canvas height use when fixSize flag is true default value is 300



```
<script>
    let parentEl = document.querySelector("body");
    /***
     *  not all options in the object are required. they been declared for show case.
     * */
    let options ={
        parentEl : parentEl,
        fullScreen: true,
        fixSize: false,
        matchParentSize:false,
        width:500,
        height:500,

    };
    window.ImageZoomComponent.init(options);
    </script>

```
